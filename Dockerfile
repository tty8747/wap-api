FROM golang:1.18.3-alpine3.16 AS builder

WORKDIR /app

ARG CGO_ENABLED=0 \
    GOARCH=amd64 \
    GOOS=linux \
    BINARY_NAME=api \
    PATH_TO_SOURCE=./cmd/api


RUN apk --no-cache add make

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY . /app/
RUN make -f Makefile build

FROM alpine:3.16

ENV DB_HOSTNAME=localhost

COPY --from=builder /app/bin/api-linux /api/api
COPY --from=builder /app/configs /api/configs

WORKDIR /api

ARG USERNAME=jarvis \
    USER_UID=11000 \
    USER_GID=11000

RUN adduser -DH -u $USER_UID -g $USER_GID $USERNAME \
    && chown -R $USER_UID:$USER_GID /api

USER $USERNAME:$USER_GID

EXPOSE 8080

CMD ["./api"]
