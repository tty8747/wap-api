variables:
  WAP_USER: ${WAP_USER}
  WAP_PASSWORD: ${WAP_PASSWORD}
  HOST_IP: 127.0.0.1
  PUBLISHED_PORT: 8080

  DOCKER_TAG: ${CI_COMMIT_SHORT_SHA}
  BUILD_NAME: wap-api
  CI_REGISTRY: registry.gitlab.com
  CI_REGISTRY_USER: tty8747

  # Environment variables for Makefile
  BINARY_NAME: api
  PATH_TO_SOURCE: ./cmd/api
  GOARCH: amd64
  GOOS: linux
  PJ_PATH: gitlab.com/tty8747/wap-api

.testing_variables: &testing_variables
  ENV: testing
  HOST_PORT: 7070

.production_variables: &production_variables
  ENV: production
  HOST_PORT: 8080

stages:
  - test
  - build
  - deploy

lint:
  image: golang:alpine3.16
  stage: test
  variables:
    REVIVE_FORCE_COLOR: 1
  script:
    - go install github.com/mgechev/revive@latest
    - revive -formatter stylish ./... | tee revive.log
  artifacts:
    name: "liner log"
    paths:
      - revive.log
    expire_in: 1 week

tests:
  stage: test
  image: golang:stretch
  script:
    - go install gotest.tools/gotestsum@latest
    - gotestsum --junitfile report.xml --format testname
  artifacts:
    when: always
    reports:
      junit: report.xml


build:
  stage: build
  image: docker:20.10.17
  script:
    - docker login registry.gitlab.com -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD}
    - apk add --no-cache jq curl
    # install trivy
    - export VERSION=$(curl --silent "https://api.github.com/repos/aquasecurity/trivy/releases/latest" | jq '.tag_name' | tr -d '"|[A-Za-z]')
    - wget https://github.com/aquasecurity/trivy/releases/download/v${VERSION}/trivy_${VERSION}_Linux-64bit.tar.gz
    - tar zxvf trivy_${VERSION}_Linux-64bit.tar.gz
    - >
      docker build
      --pull
      --label "org.opencontainers.image.title=$CI_PROJECT_TITLE"
      --label "org.opencontainers.image.version=$DOCKER_TAG"
      --tag $CI_REGISTRY/$CI_REGISTRY_USER/$BUILD_NAME:$DOCKER_TAG
      .
    - ./trivy image --exit-code 0 --severity HIGH --no-progress $CI_REGISTRY/$CI_REGISTRY_USER/$BUILD_NAME:$DOCKER_TAG
    - ./trivy image --exit-code 1 --severity CRITICAL --no-progress $CI_REGISTRY/$CI_REGISTRY_USER/$BUILD_NAME:$DOCKER_TAG
    - docker push $CI_REGISTRY/$CI_REGISTRY_USER/$BUILD_NAME:$DOCKER_TAG
  after_script:
    - docker tag  $CI_REGISTRY/$CI_REGISTRY_USER/$BUILD_NAME:$DOCKER_TAG $CI_REGISTRY/$CI_REGISTRY_USER/$BUILD_NAME:latest
    - docker push $CI_REGISTRY/$CI_REGISTRY_USER/$BUILD_NAME:latest
  allow_failure: false
  cache:
    paths:
      - $HOME/.cache/trivy
  tags:
    - docker.sock

.deploy:
  stage: deploy
  image:
    name: linuxserver/docker-compose:2.6.1-v2
    entrypoint: ["/bin/bash", "-c"]
  environment:
    name: ${ENV}
  before_script:
    - docker version
    - docker compose version
  script:
    - docker rm -f ${BUILD_NAME}_${ENV}
    - >
      docker run
      --restart always
      --name ${BUILD_NAME}_${ENV}
      -p ${HOST_PORT}:${PUBLISHED_PORT}
      -e WAP_USER=${WAP_USER}
      -e WAP_PASSWORD=${WAP_PASSWORD}
      -e HOST_IP=${HOST_IP}
      -e PUBLISHED_PORT=${PUBLISHED_PORT}
      -d ${CI_REGISTRY}/${CI_REGISTRY_USER}/${BUILD_NAME}:${DOCKER_TAG}

deploy testing:
  extends: .deploy
  variables:
    *testing_variables

deploy production:
  extends: .deploy
  variables:
    *production_variables
  when: manual
