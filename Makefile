include .env_Makefile

.DEFAULT_GOAL := all

# the same that .DEFAULT_GOAL for older version of make (<= 3.80)
# .PHONY: default
# default: clean;

.PHONY: all
all: build run clean

.PHONY:
init:
	$(info -> Only run it once!)
	go mod init ${PJ_PATH}

.PHONY:
lint:
	docker run --rm -it -v "${PWD}:/data" -w /data -e "REVIVE_FORCE_COLOR=1" ghcr.io/mgechev/revive:1.2.1 -formatter stylish ./... | tee revive.log

.PHONY:
test: test_controllers

.PHONY:
test_controllers:
	$(info -> Test controllers:)
	# go test -v ./internal/controllers/controllers_test.go
	cd internal/controllers && go test -v

.PHONY: build
build:
	$(info -> Build:)
	go mod tidy -v
	mkdir -p ./bin
	GOARCH=${GOARCH} GOOS=${GOOS} go build -o ./bin/${BINARY_NAME}-${GOOS} ${PATH_TO_SOURCE}

.PHONY: run
run:
	$(info -> Run:)
	./bin/${BINARY_NAME}-${GOOS}

.PHONY: clean
clean:
	$(info -> Clean:)
	go clean
	rm ./bin/${BINARY_NAME}-${GOOS}
	rmdir ./bin

.PHONY: messages
messages:
	$(info test info message)
	$(warning test warning message)
	$(error test error message)

