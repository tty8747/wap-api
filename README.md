# WAP API (wap-api)

WAP (Without any paper) - Application Programming Interface to connect frontend (various client interface) to database, also containing useful functions.

## How to start:

Set env variables:
```bash
export WAP_USER="yourusername"
export WAP_PASSWORD="yourstrongpassword"
```

Also put **server**, **port** and **database** name into `config.yml`

Finally run:
```bash
make
```

> Install [mssql-tools](https://docs.microsoft.com/ru-ru/sql/linux/sql-server-linux-setup-tools?view=sql-server-ver16#install-tools-on-ubuntu-2004)
> `sqlcmd -S <your_server>.database.windows.net -U <your_username> -P <your_password> -d <your_database> -i ./CreateTestData.sql
`

## Run with `docker compose`:

- Set env variables with `export` or set it in `.env` file
- Run `docker compose up`
