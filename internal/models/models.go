package models

// type OrderStatus struct {
// 	TaskAccountNum, ClientName, OptimName, PosState, DateComplete string
// 	PosNum, NCount, NCountForManufakt, NCountManufakted           int
// }

type OrderStatus struct {
	AccountNum, VEKA string
	Total, Ready     int
}

type FurnSet01 struct {
	ID int `json:"id,omitempty" validate:"required"`
}
