package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/tty8747/wap-api/internal/controllers"
)

func Route(app *fiber.App) {
	app.Get("/v1/help", controllers.Help)
	app.Get("/v1/help/:yourVar2", controllers.Help)
	app.Get("/v1/gag", controllers.Gag)
	app.Get("/v1/getorderstatus/*", controllers.GetOrderStatus)
	app.Get("/v1/set01/*", controllers.Set01)
}
