package controllers

import (
	"bytes"
	"context"
	"database/sql"
	"fmt"
	"image"
	"image/jpeg"
	"log"
	"os"
)

// ReadIDTable reads all of furniture connected with order id
func ReadIDTable(id string) error {
	ctx := context.Background()

	// Check if database is alive.
	if err := db.PingContext(ctx); err != nil {
		return err
	}

	tsql := fmt.Sprintf(`
		select
		  nCount,
		  MatName,
		  ConturNum
		from
		  dbo.f_GetListFurnit_Contur(%s, 0)
		order by
		  ConturNum,
		  Art,
		  MatName
`, id)

	// Execute query
	rows, err := db.QueryContext(ctx, tsql)
	if err != nil {
		return err
	}
	defer rows.Close()

	// Iterate through the result set.
	for rows.Next() {
		var nCount, matName, conturNum string
		var count int

		// Get values from row.
		err := rows.Scan(&nCount, &matName, &conturNum)
		if err != nil {
			return err
		}

		fmt.Printf("nCount: %s, MatName: %s, ConturNum: %s\n", nCount, matName, conturNum)
		count++
	}

	switch {
	case err == sql.ErrNoRows:
		log.Printf("no order with BarCode %d\n", 1000501352)
	case err != nil:
		log.Fatalf("query error: %v\n", err)
	default:
		log.Printf("IDTable:\n%s\n", "!")
	}

	return nil
}

func ReadDrawing() error {
	ctx := context.Background()

	// Check if database is alive.
	if err := db.PingContext(ctx); err != nil {
		return err
	}

	tsql := fmt.Sprintf(`
    Select
      Plot
    from
      Plot
    where
      idProject = 2106603
      and nGroup = 1
      and isNull(bGPPlot, 0) = 0
`)
	var result []byte

	// Execute query
	err := db.QueryRowContext(ctx, tsql).Scan(&result)
	switch {
	case err == sql.ErrNoRows:
		log.Printf("no order with idProject %d\n", 2106603)
	case err != nil:
		log.Fatalf("query error: %v\n", err)
	default:
		log.Printf("idTask is %b\n", result)
	}

	serveFrames(result)
	return nil
}

func serveFrames(imgByte []byte) {

	img, _, err := image.Decode(bytes.NewReader(imgByte))
	if err != nil {
		log.Fatalln(err)
	}

	out, _ := os.Create("./img.jpeg")
	defer out.Close()

	var opts jpeg.Options
	opts.Quality = 100

	err = jpeg.Encode(out, img, &opts)
	//jpeg.Encode(out, img, nil)
	if err != nil {
		log.Println(err)
	}
}
