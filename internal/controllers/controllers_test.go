package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http/httptest"
	"net/url"
	"os"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/stretchr/testify/assert"
	_ "gitlab.com/tty8747/wap-api/internal/auth"
)

// Script to get data.json:
// SELECT TOP 2000
//   Task.AccountNum,
//   -- CONVERT(VARCHAR, Task.DateComplite, 103) AS [DateComplite],
//   ISNULL(Task.nCountWindow, 0) AS [Total],
//   ISNULL(Task.nCountDepDebet, 0) AS [Ready],
//   dbo.f_GetTaskProfSysList_RZ(Task.ID) AS [VEKA]
// FROM
//   Task
// WHERE
//   1 = 1
//   -- AND CONVERT(VARCHAR, Task.DateComplite, 103) = CONVERT(VARCHAR, '23/10/2022', 103)
//   AND dbo.f_GetTaskProfSysList_RZ(Task.ID) like '%VEKA%'
//   -- AND Task.AccountNum = '147985/m' -- not ready
//   -- AND Task.AccountNum = '26473/15' -- ready

func TestGetOrderStatusFromFile(t *testing.T) {
	// file with testing data
	const testingData = "./data.json"

	// Order struct which contains a number of order,
	// total and ready parts of order
	type Order struct {
		AccountNum string `json:"accountNum"`
		Total      int    `json:"total"`
		Ready      int    `json:"ready"`
	}

	// Orders struct which contains
	// an array of orders
	type Orders struct {
		Orders []Order `json:"orders"`
	}

	// Open the `data.json` file
	content, err := os.Open(testingData)
	if err != nil {
		log.Fatal("Error when opening file: ", err)
	}
	log.Printf("Successfully opened file: %s", testingData)
	defer content.Close()

	// Read our opened jsonFile as a byte array.
	byteValue, _ := ioutil.ReadAll(content)

	// Initialize our Users array
	var orders Orders

	// we unmarshal our byteArray which contains our
	// jsonFile's content into 'users' which we defined above
	json.Unmarshal(byteValue, &orders)

	// Define Fiber app
	app := fiber.New()

	// Comment because of `import cycle not allowed in test`
	// routes.Route(app)

	app.Get("/v1/getorderstatus/*", GetOrderStatus)

	// we iterate through every user within our users array and
	// print out the user Type, their name, and their facebook url
	// as just an example
	for i := 0; i < len(orders.Orders); i++ {
		// fmt.Println("AccountNum: " + orders.Orders[i].AccountNum)
		// fmt.Println("Total:", orders.Orders[i].Total)
		// fmt.Println("Ready:", orders.Orders[i].Ready)

		// Create a new http request with the route from the test case
		route := fmt.Sprintf("/v1/getorderstatus/%s", url.QueryEscape(orders.Orders[i].AccountNum))
		req := httptest.NewRequest("GET", route, nil)

		// Perform the request plain with the app,
		// the second argument is a request latency
		// (set to -1 for no latency)
		resp, _ := app.Test(req, -1)

		// Read the response body
		body, err := ioutil.ReadAll(resp.Body)

		// Ensure that the body was read correctly
		assert.Nilf(t, err, "Error reading body")

		if orders.Orders[i].Total == orders.Orders[i].Ready {
			// Verify, that the reponse body equals the expected body
			assert.Equalf(t, "\"OK\"", string(body), "get order with OK status")
			if "\"OK\"" != string(body) {
				log.Println("Value from api:", string(body), "Value from file:", orders.Orders[i].Total, "==", orders.Orders[i].Ready, "Order:", orders.Orders[i].AccountNum)
			}
		} else {
			// Verify, that the reponse body equals the expected body
			assert.Equalf(t, "\"F\"", string(body), "get not ready order")
			if "\"F\"" != string(body) {
				log.Println("Value from api:", string(body), "Value from file:", orders.Orders[i].Total, "!=", orders.Orders[i].Ready, "Order:", orders.Orders[i].AccountNum)
			}
		}
		// Print number of handled orders
		if i == (len(orders.Orders) - 1) {
			log.Printf("*** %d orders handled\n", i+1)
		}
	}
}

/*
func for memorize
func TestGetOrderStatus(t *testing.T) {
	// Define a structure for specifying input and output data
	// of a single test case
	tests := []struct {
		description  string // description of the test case
		route        string // route path to test
		expectedCode int    // expected HTTP status code
		expectedBody string // body response
	}{
		// First test case
		{
			description:  "get not ready order",
			route:        "/v1/getorderstatus/212769",
			expectedCode: http.StatusOK,
			expectedBody: "\"F\"",
		},
		// Second test case
		{
			description:  "get not found order",
			route:        "/v1/getorderstatus/000000",
			expectedCode: http.StatusNotFound,
			expectedBody: "\"NF\"",
		},
		// Third test case
		{
			description:  "get order with OK status",
			route:        "/v1/getorderstatus/281470",
			expectedCode: http.StatusOK,
			expectedBody: "\"OK\"",
		},
		{
			description:  "get complicated order",
			route:        "/v1/getorderstatus/281470/m",
			expectedCode: http.StatusOK,
			expectedBody: "\"OK\"",
		},
	}

	// Define Fiber app
	app := fiber.New()
	routes.Route(app)

	// Iterate through test single test cases
	for _, test := range tests {
		// Create a new http request with the route from the test case
		req := httptest.NewRequest("GET", test.route, nil)

		// Perform the request plain with the app,
		// the second argument is a request latency
		// (set to -1 for no latency)
		resp, _ := app.Test(req, -1)

		// Verify, if the status code is as expected
		assert.Equalf(t, test.expectedCode, resp.StatusCode, test.description)
		// if !(reflect.DeepEqual(test.expectedCode, resp.StatusCode)) {
		// 	t.Fatal(test.description)
		// }

		// Read the response body
		body, err := ioutil.ReadAll(resp.Body)

		// Ensure that the body was read correctly
		assert.Nilf(t, err, test.description)
		// Verify, that the reponse body equals the expected body
		assert.Equalf(t, test.expectedBody, string(body), test.description)
	}
}
*/
