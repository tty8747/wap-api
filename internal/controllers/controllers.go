package controllers

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"net/url"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/tty8747/wap-api/internal/auth"
	"gitlab.com/tty8747/wap-api/internal/dbprovider"
	"gitlab.com/tty8747/wap-api/internal/models"
	"gitlab.com/tty8747/wap-api/internal/responses"
)

// Connect to database
var db *sql.DB = dbprovider.Connect(auth.ReturnConnString())

func Help(c *fiber.Ctx) error {
	const help = `Example commands:`
	var yourVar1 = c.Query("yourVar1", "You didn't set yourVar1")
	var yourVar2 = c.Params("yourVar2", "You didn't set yourVar2")
	var h = c.Hostname()
	response := fmt.Sprintf(`
    Hostname: %s

    yourVar1: %s
    yourVar2: %s

    Examples:
      curl -D - -s -X GET "http://%s/v1/help"
      curl -D - -s -X GET "http://%s/v1/help?yourVar1=hello"
      curl -D - -s -X GET "http://%s/v1/help/hello"

      curl -D - -s -X GET "http://%s/v1/getorderstatus/212769"
`, h, yourVar1, yourVar2, h, h, h, h)
	return c.SendString(response)
}

func Gag(c *fiber.Ctx) error {
	return c.Status(http.StatusOK).JSON(responses.ResponseSet01{Status: http.StatusOK, Message: "success", Data: &fiber.Map{"object": `{Color: red, Size: large, Count: 4}`}})
}

func GetOrderStatus(c *fiber.Ctx) error {
	ctx := context.Background()

	// Check if database is alive
	if err := db.PingContext(ctx); err != nil {
		return err
	}

	response := responses.OrderStatus

	// order := c.Query("order", "nil")
	order, _ := url.QueryUnescape(c.Params("*", "nil"))
	if order == "nil" {
		return c.Status(http.StatusNotAcceptable).JSON(response["var not set"])
	}

	tsql := fmt.Sprintf(`
SELECT
  Task.AccountNum,
  ISNULL(Task.nCountWindow, 0) AS [Total],
  ISNULL(Task.nCountDepDebet, 0) AS [Ready],
  dbo.f_GetTaskProfSysList_RZ(Task.ID)
FROM
  Task
WHERE
  1 = 1
  AND dbo.f_GetTaskProfSysList_RZ(Task.ID) like '%%VEKA%%'
  AND Task.AccountNum = '%s'
`, order)

	rows, err := db.QueryContext(ctx, tsql)
	if err != nil {
		return err
	}
	defer rows.Close()

	// Iterate through the result set
	for rows.Next() {
		// Get values from row
		var ordSt models.OrderStatus
		err := rows.Scan(
			&ordSt.AccountNum,
			&ordSt.Total,
			&ordSt.Ready,
			&ordSt.VEKA)
		if err != nil {
			return err
		}

		//	log.Println(
		//		ordSt.AccountNum,
		//		ordSt.Total,
		//		ordSt.Ready,
		//		ordSt.VEKA)

		if ordSt.Total == ordSt.Ready {
			return c.Status(http.StatusOK).JSON(response["ready"])
		} else if ordSt.Total != ordSt.Ready {
			return c.Status(http.StatusOK).JSON(response["not ready"])
		}
	}
	return c.Status(http.StatusOK).JSON(response["not found"])
}

func Set01(c *fiber.Ctx) error {
	ctx := context.Background()

	// Check if database is alive
	if err := db.PingContext(ctx); err != nil {
		return err
	}

	result := models.FurnSet01{
		ID: 0,
	}

	return c.Status(http.StatusOK).JSON(responses.ResponseSet01{Status: http.StatusOK, Message: "Success", Data: &fiber.Map{"data": result}})
}
