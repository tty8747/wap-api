package auth

import (
	"fmt"
	"log"

	. "github.com/spf13/viper"
)

var configPaths []string = []string{"./configs", "./../configs", "./../../configs"}

func ReturnConnString() string {
	// bind env variables
	SetEnvPrefix("wap") // will be uppercased automatically
	BindEnv("user")
	BindEnv("password")

	// config file
	for _, elem := range configPaths {
		AddConfigPath(elem)
	}
	SetConfigName("config")
	SetConfigType("yaml")

	// get login and password from env variables
	user := Get("user")
	password := Get("password")

	if user == nil {
		user = "Not set"
	}

	// to aviod password with nil value
	if password == nil {
		password = ""
	}

	// check if configfile is available
	if err := ReadInConfig(); err != nil {
		log.Println("No configuration file loaded")
		log.Fatal(err)
	}

	// get variables from config file
	host := GetString("db.host")
	port := GetInt("db.port")
	dbname := GetString("db.name")
	encrypt := GetString("db.encrypt")

	fmt.Printf("\nCheck env variables:\n\tWAP_USER: %s\n\tWAP_PASSWORD: %s\n", user, "hidden")
	fmt.Printf("\nCheck config variables:\n\tDB host: %s\n\tDB port: %d\n\tDB name: %s\n\n", host, port, dbname)

	// Build connection string
	connString := fmt.Sprintf("server=%s;user id=%s;password=%s;port=%d;database=%s;encrypt=%s;", host, user, password, port, dbname, encrypt)

	return connString
}

func ReturnPointToListen() string {
	// config file
	for _, elem := range configPaths {
		AddConfigPath(elem)
	}
	SetConfigName("config")
	SetConfigType("yaml")

	// check if configfile is available
	if err := ReadInConfig(); err != nil {
		log.Println("No configuration file loaded")
		log.Fatal(err)
	}

	// get variables from config file

	apiHost := GetString("api.host")
	apiPort := GetInt("api.port")

	fmt.Printf("\nCheck config variables:\n\tAPI host: %s\n\tAPI port: %d\n\n", apiHost, apiPort)

	return fmt.Sprintf("%s:%d", apiHost, apiPort)
}
