package responses

import "github.com/gofiber/fiber/v2"

// Create reusable struct to describe API's response
type ResponseSet01 struct {
	Status  int        `json:"status"`
	Message string     `json:"message"`
	Data    *fiber.Map `json:"data"`
}

var OrderStatus map[string]string = map[string]string{
	"ready":       "OK",
	"not ready":   "F",
	"not found":   "NF",
	"var not set": "NS",
}
