package dbprovider

import (
	"context"
	"database/sql"
	"log"

	_ "github.com/microsoft/go-mssqldb"
)

func Connect(connString string) *sql.DB {
	var db *sql.DB
	var err error

	// Create connection pool
	db, err = sql.Open("sqlserver", connString)
	if err != nil {
		log.Fatal("Error creating connection pool: ", err.Error())
	}
	ctx := context.Background()
	if err := db.PingContext(ctx); err != nil {
		log.Fatal("Ping db error: ", err.Error())
	}
	log.Printf("Connected to database!\n")

	return db
}
