package main

import (
	"log"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/tty8747/wap-api/internal/auth"
	"gitlab.com/tty8747/wap-api/internal/routes"
)

func main() {

	app := setup()

	// Start listen
	pointToListen := auth.ReturnPointToListen()
	if err := app.Listen(pointToListen); err != nil {
		log.Fatal(err.Error())
	}
}

func setup() *fiber.App {
	// Start api server
	app := fiber.New()

	// Attach the newly created route to the http.Server
	routes.Route(app)

	return app
}
